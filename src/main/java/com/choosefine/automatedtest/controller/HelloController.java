package com.choosefine.automatedtest.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 潘钱钦（qianqinpan@outlook.com）
 * @create 2017-07-03 16:00
 **/
@RestController
public class HelloController {

    @GetMapping("say")
    public String say(){
        return "Hello World";
    }
}
