package com.choosefine.automatedtest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.HttpMethod;
import org.springframework.web.bind.annotation.RequestMapping;

@SpringBootApplication
public class AutomatedTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(AutomatedTestApplication.class, args);
	}
	
}
